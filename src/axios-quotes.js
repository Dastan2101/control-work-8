import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://burger-project-dastan.firebaseio.com/'
});

export default instance;