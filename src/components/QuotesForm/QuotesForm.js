import React, {Component} from 'react';
import {CATEGORIES} from '../CategoriesTitle/CategoriesTitle';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class QuotesForm extends Component {

    state = {
        category: CATEGORIES.map(category => category.id)[0],
        quote: '',
        author: '',
    };


    changeHandler = (event) => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    submitHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});

    };

    render() {
        return (
            <Form className="MainForm" onSubmit={this.submitHandler}>
                <FormGroup row>
                    <Label for="category" sm={2}>Quotes Categories</Label>
                    <Col sm={10}>
                        <Input type="select" name="category" id="category"
                               onChange={this.changeHandler}
                               value={this.state.category}
                        >
                            {CATEGORIES.map(category => (
                                <option key={category.id} value={category.id}>{category.title}</option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="name" sm={2}>Author</Label>
                    <Col sm={10}>
                        <Input type="text" name="author"
                               value={this.state.author}
                               onChange={this.changeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="name" sm={2}>Quote text</Label>
                    <Col sm={10}>
                        <Input type="textarea" name="quote"
                               value={this.state.quote}
                               onChange={this.changeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{size: 10, offset: 2}}>
                        <Button color="danger" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default QuotesForm;