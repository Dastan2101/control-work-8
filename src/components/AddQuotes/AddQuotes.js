import React, {Component, Fragment} from 'react';
import axios from '../../axios-quotes';
import QuotesForm from "../QuotesForm/QuotesForm";

class AddQuotes extends Component {

    addQuote = quotes => {

        axios.post('quotes.json', quotes).then(() => {
            this.props.history.replace('/')
        })
    };

    render() {
        return (
            <Fragment>
                <h2>Submit new quote</h2>
                <QuotesForm onSubmit={this.addQuote} />
            </Fragment>
        );
    }
}

export default AddQuotes;