import React, {Component} from 'react';
import axios from '../../axios-quotes';
import {Button, Input} from "reactstrap";
import {CATEGORIES} from "../CategoriesTitle/CategoriesTitle";

class EditPage extends Component {

    state = {
        quote: '',
        author: '',
        category: ''
    };


    changeHandler = event => {
        const quote = event.target.name;
        this.setState({[quote]: event.target.value})
    };

    componentDidMount() {

        axios.get('quotes/' + this.props.match.params.id + '.json').then(response => {
            this.setState({quote: response.data.quote, author: response.data.author, category: response.data.category});
        })
    }


    saveChanged = (event) => {

        event.preventDefault();

        let change = {
            quote: this.state.quote,
            author: this.state.author,
            category: this.state.category,
        };
        axios.put('quotes/' + this.props.match.params.id + '.json', change).then(() => {
            this.props.history.replace('/')
        })

    };

    render() {

        return (
            <form onSubmit={this.saveChanged}>
                <Input value={this.state.quote} onChange={this.changeHandler} name="quote"/>
                <Input value={this.state.author} onChange={this.changeHandler} name="author"/>
                <Input type="select" name="category" id="category"
                       onChange={this.changeHandler}
                       value={this.state.category}
                >
                    {CATEGORIES.map(category => (
                        <option key={category.id} value={category.id}>{category.title}</option>
                    ))}
                </Input>
                <Button color="info">Save</Button>
            </form>
        );
    }
}

export default EditPage;