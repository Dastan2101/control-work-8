import React, {Component} from 'react';
import axios from "../../axios-quotes";
import Row from "reactstrap/es/Row";
import {
    Button,
    Card,
    CardBody, CardColumns,
    CardFooter,
    CardHeader,
    CardText,
    CardTitle,
    Col,
    Nav,
    NavItem,
    NavLink
} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import {CATEGORIES} from "../CategoriesTitle/CategoriesTitle";

class QuotesPage extends Component {

    state = {
        quotes: null,
    };

    getData = () => {
        let url = 'quotes.json';

        const quotesId = this.props.match.params.quotesId;

        if (quotesId) {
            url += `?orderBy="category"&equalTo="${quotesId}"`;

        }

        axios.get(url).then(response => {
            const quotes = Object.keys(response.data).map(id => {
                return {...response.data[id], id}

            });
            this.setState({quotes})
        }).catch(error => {
            return error;
        });


    };

    componentDidMount() {
        this.getData()
    };

    deleteQuote = (key) => {
        axios.delete('quotes/' + key + '.json').then(() => {
            this.getData();
        }).catch(error => {
            return error
        });

    };

    componentDidUpdate(prevProps) {
        if (this.props.match.params.quotesId !== prevProps.match.params.quotesId) {
            this.getData();
        }
    }


    render() {

        let quotes = null;

        if (this.state.quotes) {
            quotes = this.state.quotes.map(quoteInfo => (

                <Card key={quoteInfo.id}>
                    <CardHeader>{quoteInfo.category.toUpperCase()}</CardHeader>
                    <CardBody>
                        <CardTitle><b>Author: <i>{quoteInfo.author}</i></b></CardTitle>
                        <CardText>{quoteInfo.quote}</CardText>
                    </CardBody>
                    <CardFooter>
                        <NavLink tag={RouterNavLink} to={'/quotes/' + quoteInfo.id + '/edit'}>Edit</NavLink>
                        <Button onClick={() => this.deleteQuote(quoteInfo.id)}>Delete</Button>
                    </CardFooter>
                </Card>
            ))
        }

        return (
            <Row>
                <Col sm={3} style={{marginTop: '40px'}}>
                    <h5>Products by category:</h5>
                    <Nav vertical>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/" exact>All</NavLink>
                        </NavItem>
                        {CATEGORIES.map(category => (
                            <NavItem key={category.id}>
                                <NavLink
                                    tag={RouterNavLink}
                                    to={"/quotes/" + category.id}
                                    exact
                                >
                                    {category.title}</NavLink>
                            </NavItem>
                        ))}
                    </Nav>
                </Col>
                <Col sm={9} style={{marginTop: '40px'}}>
                    <CardColumns>
                        {quotes}
                    </CardColumns>
                </Col>
            </Row>
        );
    }
}

export default QuotesPage;