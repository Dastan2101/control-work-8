import React, {Component, Fragment} from 'react';
import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {Route, Switch, NavLink as RouterNavLink} from "react-router-dom";
import QuotesPage from "../components/QuotesPage/QuotesPage";
import AddQuotes from "../components/AddQuotes/AddQuotes";
import EditPage from "../components/EditPage/EditPage";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Navbar dark color="dark" light expand="md">
                    <NavbarBrand color="light" expand="md">Quotes Central</NavbarBrand>
                    <NavbarToggler/>
                    <Collapse isOpen navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink tag={RouterNavLink} to="/" exact>Quotes</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink tag={RouterNavLink} to="/add">Submit new quotes</NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
                <Container>
                    <Switch>
                        <Route path="/" exact component={QuotesPage}/>
                        <Route path="/add" component={AddQuotes}/>
                        <Route path="/quotes/:id/edit" component={EditPage}/>
                        <Route path="/quotes/:quotesId" component={QuotesPage}/>
                    </Switch>
                </Container>
            </Fragment>
        );
    }
}

export default App;
